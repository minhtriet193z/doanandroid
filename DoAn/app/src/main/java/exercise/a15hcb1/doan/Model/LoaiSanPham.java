package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 01/02/2017.
 */

public class LoaiSanPham {
    private Integer id;
    private String name;
    private String alias;
    private String description;
    private String hinh;

    public LoaiSanPham(Integer id, String alias, String name, String description, String hinh) {
        this.id = id;
        this.alias = alias;
        this.name = name;
        this.description = description;
        this.hinh = hinh;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }
}
