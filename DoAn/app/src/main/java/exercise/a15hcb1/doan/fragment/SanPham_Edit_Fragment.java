package exercise.a15hcb1.doan.fragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.SanPham;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.SanPhamEditAdapter;

/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class SanPham_Edit_Fragment extends Fragment {
    public List<SanPham> mangSanPham;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    View rootView;
    public static SanPham_Edit_Fragment newInstance() {
        SanPham_Edit_Fragment fm = new SanPham_Edit_Fragment();
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sanpham_edit_fragment, container, false);

        mangSanPham = new ArrayList<SanPham>();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_sanpham_edit);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        SharedPreferences user = rootView.getContext().getSharedPreferences(MainActivity.getFileUser(),0);
        Integer id = user.getInt("id",0);

        new loadSanPham().execute(getString(R.string.url)+"UserShowSanPham/"+id);

        return rootView;
    }


    public class loadSanPham extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    mangSanPham.add(new SanPham(
                            object.getInt("id"),
                            object.getString("name"),
                            object.getInt("gia"),
                            object.getString("hinh"),
                            object.getString("diachi"),
                            object.getString("chitiet"),
                            object.getString("tinhtrang"),
                            object.getInt("view"),
                            object.getInt("user_id"),
                            object.getInt("loai_id")
                    ));
                }
                SanPhamEditAdapter adapter = new SanPhamEditAdapter(mangSanPham, rootView.getContext());
                recyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
