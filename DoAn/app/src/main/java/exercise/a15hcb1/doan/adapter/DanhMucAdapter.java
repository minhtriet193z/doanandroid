package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.LoaiSanPham;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 01/02/2017.
 */

public class DanhMucAdapter extends RecyclerView.Adapter<DanhMucAdapter.RecyclerViewHolder> {
    List<LoaiSanPham> myDanhMuc;
    Context context;
    public DanhMucAdapter(List<LoaiSanPham> myDanhMuc, Context context) {
        this.myDanhMuc = myDanhMuc;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.danhmuc_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        String name  = myDanhMuc.get(position).getName();
        holder.txtDMItem.setText(name);
        String imageName = myDanhMuc.get(position).getHinh();
        imageName  = imageName.replace(".jpg","").trim();
        int resID = this.context.getResources().getIdentifier(imageName, "drawable", this.context.getPackageName());
        holder.ivDMIcon.setImageResource(resID);
    }

    @Override
    public int getItemCount() {
        return myDanhMuc.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView ivDMIcon;
        TextView txtDMItem;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ivDMIcon = (ImageView) itemView.findViewById(R.id.ivDMIcon);
            txtDMItem = (TextView) itemView.findViewById(R.id.txtDMItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), myDanhMuc.get(getAdapterPosition()).getName(),Toast.LENGTH_SHORT).show();
            SharedPreferences settings = context.getSharedPreferences(MainActivity.getDanhMuc(),0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("id",myDanhMuc.get(getAdapterPosition()).getId());
            editor.commit();

            MainActivity main = (MainActivity) context;
            ((MainActivity) context).replaceHomeFragment();
        }
    }

    public void addItem(int position, LoaiSanPham danhmuc){
        myDanhMuc.add(position,danhmuc);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        myDanhMuc.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<LoaiSanPham> list){
        this.myDanhMuc = list;
        notifyDataSetChanged();
    }
}
