package exercise.a15hcb1.doan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import exercise.a15hcb1.doan.Model.Group;
import exercise.a15hcb1.doan.Model.Message;
import exercise.a15hcb1.doan.R;

/**
 * Created by Dell 3542 on 27/01/2017.
 */

public class Chat_Fragment extends Fragment {
    Group group;
    View rootView;
    ListView listChat;
    List<Message> mangMessage;
    DatabaseReference mData;
    public static Chat_Fragment newInstance(Group group) {
        Chat_Fragment fm = new Chat_Fragment();
        fm.group = group;
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.chat_room, container, false);

        return rootView;
    }
}
