package exercise.a15hcb1.doan;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import exercise.a15hcb1.doan.Model.Message;
import exercise.a15hcb1.doan.adapter.ChatAdapter;

public class ChatRoom extends AppCompatActivity implements View.OnClickListener{
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    List<Message> mangMessage;
    DatabaseReference mData;
    ChatAdapter adapter;
    Button btnSend;
    EditText edMsg;
    String groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_room);

        /*

        listChat = (ListView) findViewById(R.id.lv_chat_rom);
        mangMessage = new ArrayList<Message>();
        mangMessage.add(new Message(1, "a", "abc"));
        mangMessage.add(new Message(1, "abc", "abc"));
        adapter = new ChatAdapter(this, R.layout.chat_room_item, mangMessage);


        listChat.setAdapter(adapter);
        */
        btnSend = (Button) findViewById(R.id.btn_chat_send);
        btnSend.setOnClickListener(this);
        edMsg = (EditText) findViewById(R.id.ed_chat_msg);

        mData = FirebaseDatabase.getInstance().getReference();

        recyclerView = (RecyclerView) findViewById(R.id.rv_chat_rom);
        layoutManager =  new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);


        mangMessage = new ArrayList<Message>();

        SharedPreferences user_file = getSharedPreferences(MainActivity.getFileUser(), 0);
        Integer user_id = user_file.getInt("id",0);

        adapter = new ChatAdapter(mangMessage,this);

        recyclerView.setAdapter(adapter);


        SharedPreferences group_chat = getSharedPreferences(MainActivity.getGroup_Chat(), 0);
        Integer group_id = group_chat.getInt("id",0);

        //lay du lieu ve sau do hien len;
        groupName = "Group" + group_id;

        mData.child(groupName).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Toast.makeText(getApplicationContext(), dataSnapshot.getValue().toString(),Toast.LENGTH_SHORT).show();
                List<Message> mangMessageTam = new ArrayList<Message>();
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    Message message = messageSnapshot.getValue(Message.class);
                    mangMessageTam.add(message);
                }
                int mangMessage_count = mangMessage.size();
                for (int i = mangMessage_count; i < mangMessageTam.size(); i++){
                    Message message = mangMessageTam.get(i);
                    mangMessage.add(message);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_chat_send){
            SharedPreferences file_User_Login = getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_send_id = file_User_Login.getInt("id",0);

            DateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
            String date = df.format(Calendar.getInstance().getTime());

            Message msg = new Message();
            msg.setMessage(edMsg.getText().toString());
            msg.setTime(date);
            msg.setUser_id(user_send_id);

            mData.child(groupName).push().setValue(msg);

            SharedPreferences group_chat = getSharedPreferences(MainActivity.getGroup_Chat(), 0);
            Integer user_receive_id = group_chat.getInt("user_receive_id",0);

            //user receive trong group la user dang dang nhap
            if(user_receive_id != user_send_id){
                new layTokenVaSend().execute(getString(R.string.url) + "User/" + user_receive_id);
            }else {
                //vi trong group luon chi co 1 user nhan. nen doi user lai
                user_receive_id = group_chat.getInt("user_send_id",0);
                new layTokenVaSend().execute(getString(R.string.url) + "User/" + user_receive_id);
            }
        }
    }


    public class layTokenVaSend extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                //tr ve user sau khi lay thong tin -> lay token_id
                JSONObject object = new JSONObject(s);

                SharedPreferences group_chat = getSharedPreferences(MainActivity.getGroup_Chat(), 0);
                SharedPreferences.Editor editor = group_chat.edit();

                editor.putString("user_receive_token",object.getString("chat_id"));

                editor.commit();

                new sendnotification().execute(getString(R.string.url)+"UserSendMessager");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    public class sendnotification extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = POST_NOTIFICATION(params[0], "POST");
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(), "Da gui tin nhan", Toast.LENGTH_SHORT).show();
        }
    }


    private String POST_NOTIFICATION(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        SharedPreferences group_chat = getSharedPreferences(MainActivity.getGroup_Chat(), 0);


        String user_receive_token = group_chat.getString("user_receive_token","");

        // URL của trang web nhận request
        HttpPost httpPost = new HttpPost(url);

        // Các tham số truyền
        List nameValuePair = new ArrayList(4);

        nameValuePair.add(new BasicNameValuePair("title", groupName));

        nameValuePair.add(new BasicNameValuePair("message", edMsg.getText().toString()));

        nameValuePair.add(new BasicNameValuePair("user_receive_token", user_receive_token));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
