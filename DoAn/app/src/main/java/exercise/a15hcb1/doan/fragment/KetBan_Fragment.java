package exercise.a15hcb1.doan.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.Model.User;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.NguoiDungAdapter;

/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class KetBan_Fragment extends Fragment {
    public List<User> mangUser;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    View rootView;

    public static KetBan_Fragment newInstance() {
        KetBan_Fragment fm = new KetBan_Fragment();
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.ketban_fragment, container, false);

        mangUser = new ArrayList<User>();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_ketban);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        new loadAllUser().execute(getString(R.string.url) + "User");

        return rootView;
    }


    public class loadAllUser extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i = 0; i< jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    mangUser.add(new User(
                            object.getInt("id"),
                            object.getString("facebook_id"),
                            object.getString("name"),
                            object.getString("phone"),
                            object.getString("chat_id"),
                            object.getString("email")
                    ));

                    NguoiDungAdapter adapter = new NguoiDungAdapter(mangUser, getContext());
                    recyclerView.setAdapter(adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
