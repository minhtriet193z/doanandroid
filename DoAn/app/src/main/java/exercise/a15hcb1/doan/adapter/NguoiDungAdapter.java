package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.User;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 03/02/2017.
 */

public class NguoiDungAdapter extends RecyclerView.Adapter<NguoiDungAdapter.RecyclerViewHolder> {
    List<User> mangUser;
    Context context;
    public NguoiDungAdapter(List<User> mangUser, Context context) {
        this.mangUser = mangUser;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.ketban_item,parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvName.setText(mangUser.get(position).getName());
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView ivIcon;
        TextView tvName;
        Button btnKetban;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivKBIcon);
            tvName = (TextView) itemView.findViewById(R.id.tvKBName);
            btnKetban = (Button) itemView.findViewById(R.id.btnKB);
            btnKetban.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnKB) {
                //luu file
                SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
                SharedPreferences.Editor editor = user_friend.edit();

                editor.putInt("id",mangUser.get(getAdapterPosition()).getId());

                editor.commit();

                Integer friend_id = mangUser.get(getAdapterPosition()).getId();

                SharedPreferences user_file = context.getSharedPreferences(MainActivity.getFileUser(), 0);
                Integer user_id = user_file.getInt("id",0);

                if(user_id != friend_id) {
                    //them ban
                    new KiemTraVaThemBan().execute(context.getString(R.string.url) + "Friend/" + user_id + "/" + friend_id);
                    new KiemTraVaThemBan2().execute(context.getString(R.string.url) + "Friend/" + friend_id + "/" + user_id);
                }else {
                    Toast.makeText(context,"Ban khong the ket ban voi chinh ban", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class KiemTraVaThemBan extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if(jsonArray.length() == 0){
                    //chua co ket ban
                    new themBan().execute(context.getString(R.string.url)+"Friend");
                }else {
                    //da ket ban
                    Toast.makeText(context,"Nguoi nay da la ban", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
    public class themBan extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = POST_FRIEND(params[0],"POST");
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context,"Them ban thanh cong", Toast.LENGTH_SHORT).show();
        }
    }

    private String POST_FRIEND(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
        Integer user_friend_id = user_friend.getInt("id", 0);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_send_id = file_User_Login.getInt("id",0);

        // Các tham số truyền
        List nameValuePair = new ArrayList(3);
        nameValuePair.add(new BasicNameValuePair("user_id", user_send_id.toString()));

        nameValuePair.add(new BasicNameValuePair("friend_id", user_friend_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }



    //Them ban lan 2
    public class KiemTraVaThemBan2 extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if(jsonArray.length() == 0){
                    //chua co ket ban
                    new themBan2().execute(context.getString(R.string.url)+"Friend");
                }else {
                    //da ket ban
                    //Toast.makeText(context,"Nguoi nay da la ban", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class themBan2 extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = POST_FRIEND2(params[0],"POST");
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Toast.makeText(context,"Them ban thanh cong", Toast.LENGTH_SHORT).show();
        }
    }

    private String POST_FRIEND2(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
        Integer user_friend_id = user_friend.getInt("id", 0);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_send_id = file_User_Login.getInt("id",0);

        // Các tham số truyền
        List nameValuePair = new ArrayList(3);
        nameValuePair.add(new BasicNameValuePair("user_id", user_friend_id.toString()));

        nameValuePair.add(new BasicNameValuePair("friend_id", user_send_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }
    //


    @Override
    public int getItemCount() {
        return mangUser.size();
    }

    public void addItem(int position, User user){
        mangUser.add(position,user);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangUser.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<User> list){
        this.mangUser = list;
        notifyDataSetChanged();
    }
}
