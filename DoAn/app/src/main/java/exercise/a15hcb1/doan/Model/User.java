package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 01/02/2017.
 */

public class User {
    private Integer id;
    private String facebook_id;
    private String name;
    private String phone;
    private String chat_id;
    private String email;
    private String picture;

    public User(Integer id, String facebook_id, String name, String phone, String chat_id, String email, String picture) {
        this.id = id;
        this.facebook_id = facebook_id;
        this.name = name;
        this.phone = phone;
        this.chat_id = chat_id;
        this.email = email;
        this.picture = picture;
    }

    public User(String facebook_id, String name, String phone, String chat_id, String email, String picture) {
        this.facebook_id = facebook_id;
        this.name = name;
        this.phone = phone;
        this.chat_id = chat_id;
        this.email = email;
        this.picture = picture;
    }

    public User(Integer id, String facebook_id, String name, String phone, String chat_id, String email) {
        this.id = id;
        this.facebook_id = facebook_id;
        this.name = name;
        this.phone = phone;
        this.chat_id = chat_id;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
