package exercise.a15hcb1.doan;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import exercise.a15hcb1.doan.Model.Group;
import exercise.a15hcb1.doan.Model.Message;
import exercise.a15hcb1.doan.Model.User;
import exercise.a15hcb1.doan.fragment.AboutFragment;
import exercise.a15hcb1.doan.fragment.BanBe_Fragment;
import exercise.a15hcb1.doan.fragment.CTSanPham_Fragment;
import exercise.a15hcb1.doan.fragment.DanhMuc_Fragment;
import exercise.a15hcb1.doan.fragment.Fragment1;
import exercise.a15hcb1.doan.fragment.Fragment2;
import exercise.a15hcb1.doan.fragment.Fragment3;
import exercise.a15hcb1.doan.fragment.GroupChat_Fragment;
import exercise.a15hcb1.doan.fragment.HomeFragment;
import exercise.a15hcb1.doan.fragment.KetBan_Fragment;
import exercise.a15hcb1.doan.fragment.SPYeuThich_Fragment;
import exercise.a15hcb1.doan.fragment.SanPham_Edit_Fragment;
import exercise.a15hcb1.doan.fragment.SanPham_Fragment;
import exercise.a15hcb1.doan.fragment.SanPham_Search_Fragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    Context context;
    //luu user khi login
    static final String FILE_USER = "my_user";
    public static String getFileUser() {
        return FILE_USER;
    }

    //luu danh muc khi chon
    static final String DANH_MUC = "my_danh_muc";
    public static String getDanhMuc() {
        return DANH_MUC;
    }

    //luu san pham khi duoc chon
    public static String getSanPham() {
        return SAN_PHAM;
    }
    static final String SAN_PHAM = "my_san_pham";

    public static String getSanPhamUser() {
        return SAN_PHAM_USER;
    }
    //Luu user cua san pham khi chon
    static final String SAN_PHAM_USER = "san_pham_user";

    public static String getUserFriend() {
        return USER_FRIEND;
    }
    static final String USER_FRIEND = "user_frined";


    public static String getGroup_Chat() {
        return Group_Chat;
    }

    static final String Group_Chat = "group_chat";

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mtoggle;
    private Toolbar mToolbar;
    private NavigationView mNavView;
    private List<Fragment> myList;
    private EditText txtSearch;
    private Button btnSearch;

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    private TextView txtUserName;
    private ImageView profileImage;
    private Bitmap bmp;
    private String TAG="";
    private User UserLogin = null;

    private Button btnHome;
    private Button btnThem;

    /*Chat */
    public static List<Group> mangGroup;
    DatabaseReference mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Favorite
        btnHome = (Button) findViewById(R.id.btnHome);
        btnHome.setOnClickListener(this);
        //Them san pham
        btnThem = (Button) findViewById(R.id.btnThem);
        btnThem.setOnClickListener(this);
        //search
        txtSearch = (EditText) findViewById(R.id.editSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        //Chat
        mData = FirebaseDatabase.getInstance().getReference();
        context = this;
        mangGroup = new ArrayList<Group>();
        mNavView = (NavigationView) findViewById(R.id.navView);
        mNavView.setNavigationItemSelectedListener(this);
        txtUserName = (TextView) findViewById(R.id.tvUserNameFacebook);

        //Create main_content -> homeFragment
        myList = new ArrayList<Fragment>();
        myList.add(new HomeFragment().newInstance(createHomeDefault()));
        myList.add(new SanPham_Edit_Fragment());
        myList.add(new AboutFragment());
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, myList.get(0)).commit();
        //Create NavigationView
        createNav();
        //Logout facebook khi khoi dong ung dung
        SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("id",0);
        editor.putString("facebook_id","");
        editor.putString("name","");
        editor.putString("email","");
        editor.putString("profile_pic","");
        editor.commit();
        LoginManager.getInstance().logOut();
        //Insert phoneNumber
        if(settings.getString("phone","") == "") {
            showDialogPhoneNumber(MainActivity.this);
        }
        //logoutFacebook();
        //Dang nhap facebook
        loginFaceBook();
    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnThem){
            if(checkLogin() == true){
                Intent intent = new Intent(MainActivity.this, ThemSanPham.class);
                startActivity(intent);
            }else{
                Toast.makeText(this,"Vui lòng đăng nhập để thêm sản phẩm", Toast.LENGTH_SHORT).show();
            }

        }else if (v.getId() == R.id.btnHome){

            replaceHomeDefault();

        }else if(v.getId() == R.id.btnSearch){
            //Toast.makeText(this,txtSearch.getText(),Toast.LENGTH_SHORT).show();
            searchSanPham();
        }
    }



    public void searchSanPham() {
        replaceHomeFragmentSearch();
    }
    //Share facebook;
    public void sharePhotoToFacebook(){
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);
    }
    /*LOGIN_FACEBOOK*/
    public void loginFaceBook() {
        loginButton = (LoginButton) findViewById(R.id.btnLogin);
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //sharePhotoToFacebook();
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        Bundle bFacebookData = getFacebookData(object);

                        txtUserName = (TextView) findViewById(R.id.tvUserNameFacebook);
                        txtUserName.setText(bFacebookData.getString("last_name"));

                        UserLogin = new User(
                                bFacebookData.getString("idFacebook"),
                                bFacebookData.getString("first_name") + " " + bFacebookData.getString("last_name"),
                                "",
                                "Chat_id",
                                bFacebookData.getString("email"),
                                bFacebookData.getString("profile_pic")
                        );

                        // Save your info
                        SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("name", UserLogin.getName());
                        editor.putString("facebook_id", UserLogin.getFacebook_id());
                        editor.putString("email", UserLogin.getEmail());
                        editor.putString("profile_pic", UserLogin.getPicture());
                        editor.commit();
                        new loadFacebookAvatar().execute(UserLogin.getPicture());
                        new loadUser().execute(getString(R.string.url)+"UserTheoFacebookId/" + UserLogin.getFacebook_id());
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

            return bundle;
        }
        catch(JSONException e) {
            Log.d(TAG,"Error parsing JSON");
        }
        return null;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    //Kiem tra da login chua
    public boolean checkLogin(){
        User user = getUserLogin();
        if(user.getFacebook_id() == ""){
            return false;
        }
        return true;
    }


    public class loadFacebookAvatar extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            profileImage = (ImageView) findViewById(R.id.img_profile);
            profileImage.setImageBitmap(bmp);
        }
    }

    public void showDialogPhoneNumber(final MainActivity mainActivity) {
        final Dialog myDialog = new Dialog(MainActivity.this);

        myDialog.setTitle("Vui lòng nhập số điện thoại của bạn");

        myDialog.setContentView(R.layout.custom_dialog_box);

        final EditText editText = (EditText) myDialog.findViewById(R.id.phoneNumber);

        Button close = (Button) myDialog.findViewById(R.id.btnClose);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("phone",String.valueOf(editText.getText()));
                editor.commit();

                myDialog.dismiss();

            }
        });
        myDialog.create();
        myDialog.show();
    }

    //Lay User sau khi login
    public User getUserLogin(){
        SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
        User User = new User(
                settings.getInt("id",0),
                settings.getString("facebook_id",""),
                settings.getString("name",""),
                settings.getString("phone",""),
                settings.getString("chat_id",""),
                settings.getString("email",""),
                settings.getString("profile_pic","")
        );
        return User;
    }

    public class loadUser extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if (jsonArray.length() == 0) {
                    new insertUser().execute(getString(R.string.url)+"User");
                    Toast.makeText(MainActivity.this, "Dang nhap thanh cong", Toast.LENGTH_LONG).show();
                }else{
                    SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("id",jsonArray.getJSONObject(0).getInt("id"));
                    editor.commit();

                    replaceHomeDefault();

                    Toast.makeText(MainActivity.this, "Dang nhap thanh cong", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public class loadUser2 extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("id",jsonArray.getJSONObject(0).getInt("id"));
                editor.commit();

                replaceHomeDefault();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }

    public class insertUser extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = POST_USER(params[0],"POST");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
            settings.getString("facebook_id","");
            new loadUser2().execute(getString(R.string.url)+"UserTheoFacebookId/" + settings.getString("facebook_id",""));
        }
    }

    private String POST_USER(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        User user = getUserLogin();
        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        // Các tham số truyền
        List nameValuePair = new ArrayList(5);
        nameValuePair.add(new BasicNameValuePair("facebook_id", user.getFacebook_id()));

        nameValuePair.add(new BasicNameValuePair("name", user.getName()));

        nameValuePair.add(new BasicNameValuePair("password", ""));

        nameValuePair.add(new BasicNameValuePair("phone", user.getPhone()));

        nameValuePair.add(new BasicNameValuePair("chat_id", user.getChat_id()));

        nameValuePair.add(new BasicNameValuePair("email", user.getEmail()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    /*EndLOGIN*/
    /*LOGOUT Facebook*/

    /*End Logout*/

    public void logoutFacebook(){
        SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("id",0);
        editor.putString("facebook_id","");
        editor.putString("name","");
        editor.putString("email","");
        editor.putString("profile_pic","");
        editor.commit();
        LoginManager.getInstance().logOut();
        mDrawerLayout.closeDrawer(mNavView);
    }

    /*NAV_VIEW*/
    private void createNav() {
        mToolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mtoggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open, R.string.close);

        mDrawerLayout.addDrawerListener(mtoggle);
        mtoggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.nav_home){
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction().replace(R.id.main_content, new HomeFragment().newInstance(createHomeDefault())).commit();

            mDrawerLayout.closeDrawer(mNavView);
        } else if (item.getItemId() == R.id.nav_myproduct) {
            if(checkLogin() == true) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_content, new SanPham_Edit_Fragment()).commit();
                mDrawerLayout.closeDrawer(mNavView);
            }else{
                Toast.makeText(MainActivity.this, "Ban chua dang nhap", Toast.LENGTH_LONG).show();
            }
        } else if (item.getItemId() == R.id.nav_logout){
            SharedPreferences settings = getSharedPreferences(FILE_USER, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("id",0);
            editor.putString("facebook_id","");
            editor.putString("name","");
            editor.putString("email","");
            editor.putString("profile_pic","");
            editor.commit();
            LoginManager.getInstance().logOut();
            txtUserName = (TextView) findViewById(R.id.tvUserNameFacebook);
            txtUserName.setText("Chưa đăng nhập");
            profileImage = (ImageView) findViewById(R.id.img_profile);
            profileImage.setImageResource(R.mipmap.ic_launcher);
            mDrawerLayout.closeDrawer(mNavView);
        }
        return true;
    }
    /*ENDNAV*/

    /*Danh sach san pham*/

    public void replaceHomeDefault(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, new HomeFragment().newInstance(createHomeDefault())).commit();
        mDrawerLayout.closeDrawer(mNavView);
    }

    public void replaceHomeFragment(){

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, new HomeFragment().newInstance(createHomeSanPham())).commit();
    }

    public void replaceHomeFragmentSearch(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, new HomeFragment().newInstance(createHomeSearch())).commit();
    }

    public void replaceHomeFragmentCTSP(){


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_content, new HomeFragment().newInstance(createHomeCTSanPham())).commit();
    }

    public List<Fragment> createHomeSanPham(){
        List<Fragment> ListHome = new ArrayList<Fragment>();


        if(checkLogin() == true){
            ListHome.add(new SanPham_Fragment());
            ListHome.add(new SPYeuThich_Fragment());
            ListHome.add(new BanBe_Fragment());
            ListHome.add(new GroupChat_Fragment().newInstance(this));
            ListHome.add(new KetBan_Fragment());
        }else {
            ListHome.add(new SanPham_Fragment());
            ListHome.add(new Fragment2());
            ListHome.add(new Fragment3());
            ListHome.add(new Fragment1());
            ListHome.add(new Fragment2());
        }
        return ListHome;
    }

    public List<Fragment> createHomeDefault(){
        List<Fragment> ListHome = new ArrayList<Fragment>();
        if(checkLogin() == true) {
            ListHome.add(new DanhMuc_Fragment());
            ListHome.add(new SPYeuThich_Fragment());
            ListHome.add(new BanBe_Fragment());
            ListHome.add(new GroupChat_Fragment().newInstance(this));
            ListHome.add(new KetBan_Fragment());
        }else{
            ListHome.add(new DanhMuc_Fragment());
            ListHome.add(new Fragment2());
            ListHome.add(new Fragment3());
            ListHome.add(new Fragment1());
            ListHome.add(new Fragment2());
        }
        return ListHome;
    }

    public List<Fragment> createHomeCTSanPham(){
        List<Fragment> ListHome = new ArrayList<Fragment>();
        if(checkLogin() == true) {
            ListHome.add(new CTSanPham_Fragment().newInstance(getApplicationContext()));
            ListHome.add(new SPYeuThich_Fragment());
            ListHome.add(new BanBe_Fragment());
            ListHome.add(new GroupChat_Fragment().newInstance(this));
            ListHome.add(new KetBan_Fragment());
        }else {
            ListHome.add(new CTSanPham_Fragment().newInstance(getApplicationContext()));
            ListHome.add(new Fragment2());
            ListHome.add(new Fragment3());
            ListHome.add(new Fragment1());
            ListHome.add(new Fragment2());
        }
        return ListHome;
    }

    public List<Fragment> createHomeSearch(){
        List<Fragment> ListHome = new ArrayList<Fragment>();
        if(checkLogin() == true) {
            ListHome.add(new SanPham_Search_Fragment().newInstance(txtSearch.getText().toString()));
            ListHome.add(new SPYeuThich_Fragment());
            ListHome.add(new BanBe_Fragment());
            ListHome.add(new GroupChat_Fragment().newInstance(this));
            ListHome.add(new KetBan_Fragment());
        }else {
            ListHome.add(new SanPham_Search_Fragment().newInstance(txtSearch.getText().toString()));
            ListHome.add(new Fragment2());
            ListHome.add(new Fragment3());
            ListHome.add(new Fragment1());
            ListHome.add(new Fragment2());
        }
        return ListHome;
    }


    /*Chat mangGroup duoc set trong GroupChat_Fragment*/
    public void checknode(final String nodeName){
        //DatabaseReference root = FirebaseDatabase.getInstance().getReference();
        //DatabaseReference users = root.child("Users");
        mData.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //neu chua co node thi tao
                if (!(snapshot.hasChild(nodeName))) {
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
                    String date = df.format(Calendar.getInstance().getTime());
                    Message msg = new Message(0,"Tao room", date);
                    mData.child(nodeName).push().setValue(msg);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void taoRoomChatFireBase(){
        //Toast.makeText(this, String.valueOf(mangGroup.size()), Toast.LENGTH_SHORT).show();
        for(int i=0; i<mangGroup.size(); i++){
            final Group group = mangGroup.get(i);
            //tao node neu chua co
            checknode("Group"+group.getId());

            mData.child("Group"+group.getId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //dung de show thong bao co tin nhan moi
                    //Toast.makeText(getApplicationContext(), "Group" + group.getId(),Toast.LENGTH_SHORT).show();
                    /*
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,intent, PendingIntent.FLAG_ONE_SHOT);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
                    builder.setContentTitle("Group" + group.getId());
                    builder.setContentText("Ban co tin nhan moi");
                    builder.setAutoCancel(true);
                    builder.setSmallIcon(R.mipmap.ic_launcher);
                    builder.setContentIntent(pendingIntent);

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify(0,builder.build());
                    */
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
