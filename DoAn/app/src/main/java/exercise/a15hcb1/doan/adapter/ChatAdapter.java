package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.Message;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 02/02/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.RecyclerViewHolder> {
    List<Message> mangMessage;
    Context context;

    public ChatAdapter(List<Message> mangMessage, Context context) {
        this.mangMessage = mangMessage;
        this.context = context;
    }

    @Override
    public ChatAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.chat_room_item, parent, false);
        return new ChatAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.RecyclerViewHolder holder, int position) {
        //holder.tvChatName.setText(mangMessage.get(position).getUser_id().toString());
        holder.tvChatMsg.setText(mangMessage.get(position).getMessage());
        holder.tvChatTime.setText(mangMessage.get(position).getTime());

        SharedPreferences user_file = context.getSharedPreferences(MainActivity.getFileUser(), 0);
        Integer user_id = user_file.getInt("id",0);

        if(mangMessage.get(position).getUser_id() == user_id) {
            holder.tvChatMsg.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            holder.tvChatTime.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        }
    }

    @Override
    public int getItemCount() {
        return mangMessage.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder{

        TextView tvChatMsg, tvChatTime;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvChatMsg = (TextView) itemView.findViewById(R.id.tvChatRoomMsg);
            tvChatTime = (TextView) itemView.findViewById(R.id.tvChatRoomTime);
        }
    }

    public void addItem(int position, Message message){
        mangMessage.add(position,message);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangMessage.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<Message> list){
        this.mangMessage = list;
        notifyDataSetChanged();
    }
}
