package exercise.a15hcb1.doan;

import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Administrator on 01/01/2017.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TOKEN = "TOKEN";
    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();

        SharedPreferences settings = getSharedPreferences(MainActivity.FILE_USER, 0);
        String token = settings.getString("chat_id","");
        if(token == ""){
            settings = getSharedPreferences(MainActivity.FILE_USER, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("chat_id",recent_token);
            editor.commit();
        }
    }
}
