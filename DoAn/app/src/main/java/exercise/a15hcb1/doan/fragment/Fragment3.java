package exercise.a15hcb1.doan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 20/12/2016.
 */

public class Fragment3 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            //return super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.layout_fragment3,null);
        return v;
    }
}
