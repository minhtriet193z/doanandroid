package exercise.a15hcb1.doan.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.Group;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.GroupAdapter;

/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class GroupChat_Fragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Context context;
    List<Group> mangGroup;
    View rootView;

    public static GroupChat_Fragment newInstance(Context context) {
        GroupChat_Fragment fm = new GroupChat_Fragment();
        fm.context = context;
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.groupchat_fragment, container, false);

        mangGroup = new ArrayList<Group>();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_groupchat);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer id = file_User_Login.getInt("id",0);

        new loadGroup().execute(getString(R.string.url)+"Group/"+id);

        return rootView;
    }

    public class loadGroup extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    mangGroup.add(new Group(
                            object.getInt("id"),
                            object.getInt("user_send_id"),
                            object.getInt("user_receive_id")
                    ));
                }
                GroupAdapter adapter = new GroupAdapter(mangGroup, rootView.getContext());
                recyclerView.setAdapter(adapter);

                MainActivity main = (MainActivity) context;
                main.mangGroup = mangGroup;
                main.taoRoomChatFireBase();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
