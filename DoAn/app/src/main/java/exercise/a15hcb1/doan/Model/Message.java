package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 02/02/2017.
 */

public class Message {
    private Integer user_id;
    private String message;
    private String time;

    public Message() {
    }

    public Message(Integer user_id, String message, String time) {
        this.user_id = user_id;
        this.message = message;
        this.time = time;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
