package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.SanPham;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 02/02/2017.
 */

public class SanPhamYeuThichAdapter extends RecyclerView.Adapter<SanPhamYeuThichAdapter.RecyclerViewHolder> {
    List<SanPham> mangSanPham;
    Context context;
    public SanPhamYeuThichAdapter(List<SanPham> mangSanPham, Context context) {
        this.mangSanPham = mangSanPham;
        this.context = context;
    }

    @Override
    public SanPhamYeuThichAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.spyeuthich_item,parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SanPhamYeuThichAdapter.RecyclerViewHolder holder, int position) {
        holder.tvName.setText(mangSanPham.get(position).getName());
        holder.tvGia.setText(mangSanPham.get(position).getGia().toString());

        String image = mangSanPham.get(position).getHinh();
        String_To_ImageView(image, holder.ivIcon);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView ivIcon;
        TextView tvName, tvGia;
        Button btnXem, btnXoa;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivSPYTIcon);
            tvName = (TextView) itemView.findViewById(R.id.tvSPYTName);
            tvGia = (TextView) itemView.findViewById(R.id.tvSPYTPrice);
            btnXem = (Button) itemView.findViewById(R.id.btnSPYTXem);
            btnXoa = (Button) itemView.findViewById(R.id.btnSPYTXoa);
            btnXem.setOnClickListener(this);
            btnXoa.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnSPYTXem) {
                SharedPreferences settings = context.getSharedPreferences(MainActivity.getSanPham(), 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("id", mangSanPham.get(getAdapterPosition()).getId());
                editor.putString("name", mangSanPham.get(getAdapterPosition()).getName());
                editor.putInt("gia", mangSanPham.get(getAdapterPosition()).getGia());
                editor.putString("hinh", mangSanPham.get(getAdapterPosition()).getHinh());
                editor.putString("diachi", mangSanPham.get(getAdapterPosition()).getDiachi());
                editor.putString("chitiet", mangSanPham.get(getAdapterPosition()).getChitiet());
                editor.putString("tinhtrang", mangSanPham.get(getAdapterPosition()).getTinhtrang());
                editor.putInt("view", mangSanPham.get(getAdapterPosition()).getView());
                editor.putInt("user_id", mangSanPham.get(getAdapterPosition()).getUser_id());
                editor.putInt("loai_id", mangSanPham.get(getAdapterPosition()).getLoai_id());
                editor.commit();

                MainActivity main = (MainActivity) context;
                ((MainActivity) context).replaceHomeFragmentCTSP();

            }else if (v.getId() == R.id.btnSPYTXoa){
                Integer sanpham_id = mangSanPham.get(getAdapterPosition()).getId();

                SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
                Integer user_id = file_User_Login.getInt("id",0);

                new XoaFavorite().execute(context.getString(R.string.url)+"FavoriteXoa/" + user_id + "/" + sanpham_id);

                removeItem(getAdapterPosition());
            }
        }
    }

    public void String_To_ImageView(String strBase64, ImageView iv){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        iv.setImageBitmap(decodedByte);
    }

    @Override
    public int getItemCount() {
        return mangSanPham.size();
    }

    public void addItem(int position, SanPham sanPham){
        mangSanPham.add(position,sanPham);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangSanPham.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<SanPham> list){
        this.mangSanPham = list;
        notifyDataSetChanged();
    }

    public class XoaFavorite extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context,"Da xoa favorite", Toast.LENGTH_SHORT).show();
        }
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
