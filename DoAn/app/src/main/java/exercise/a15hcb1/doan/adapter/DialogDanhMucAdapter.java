package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.LoaiSanPham;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 02/02/2017.
 */

public class DialogDanhMucAdapter extends RecyclerView.Adapter<DialogDanhMucAdapter.RecyclerViewHolder> {
    List<LoaiSanPham> myDanhMuc = new ArrayList<LoaiSanPham>();
    Context context;

    public DialogDanhMucAdapter(List<LoaiSanPham> myDanhMuc, Context context) {
        this.myDanhMuc = myDanhMuc;
        this.context = context;
    }

    @Override
    public DialogDanhMucAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dialog_danhmuc_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DialogDanhMucAdapter.RecyclerViewHolder holder, int position) {
        holder.tvDialogDanhMucItem.setText(myDanhMuc.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return myDanhMuc.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvDialogDanhMucItem;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvDialogDanhMucItem = (TextView) itemView.findViewById(R.id.tv_dialog_danhmuc_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(),"Bạn đã chọn danh mục " + myDanhMuc.get(getAdapterPosition()).getName(),Toast.LENGTH_SHORT).show();

            SharedPreferences settings = context.getSharedPreferences(MainActivity.getDanhMuc(),0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("add_danhmuc_id",myDanhMuc.get(getAdapterPosition()).getId());
            editor.commit();
        }
    }
}
