package exercise.a15hcb1.doan.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import exercise.a15hcb1.doan.Model.LoaiSanPham;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.DanhMucAdapter;

/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class DanhMuc_Fragment extends Fragment {
    ArrayList<LoaiSanPham> mangDanhMuc;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    View rootView;

    public static DanhMuc_Fragment newInstance() {
        DanhMuc_Fragment fm = new DanhMuc_Fragment();
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.danhmuc_fragment, container, false);
        mangDanhMuc = new ArrayList<LoaiSanPham>();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_danhmuc);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //mangDanhMuc.add(new LoaiSanPham(1, "san pham 1", "san pham 1", "abc", "dienthoai.jpg"));
        //mangDanhMuc.add(new LoaiSanPham(1, "san pham 1", "san pham 1", "abc", "dienthoai.jpg"));
        new loadDanhMuc().execute(getString(R.string.url)+"LoaiSanPham");

        return rootView;
    }

    public class loadDanhMuc extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    mangDanhMuc.add(new LoaiSanPham(
                            object.getInt("id"),
                            object.getString("alias"),
                            object.getString("name"),
                            object.getString("description"),
                            object.getString("hinh")
                    ));
                }

                DanhMucAdapter adapter = new DanhMucAdapter(mangDanhMuc, getContext());
                recyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
