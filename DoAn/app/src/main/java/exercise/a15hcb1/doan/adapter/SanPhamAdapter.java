package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.SanPham;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 02/02/2017.
 */

public class SanPhamAdapter extends RecyclerView.Adapter<SanPhamAdapter.RecyclerViewHolder> {
    List<SanPham> mangSanPham;
    Context context;
    public SanPhamAdapter(List<SanPham> mangSanPham, Context context) {
        this.mangSanPham = mangSanPham;
        this.context = context;
    }

    @Override
    public SanPhamAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.sanpham_item,parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SanPhamAdapter.RecyclerViewHolder holder, int position) {
        holder.tvName.setText(mangSanPham.get(position).getName());
        holder.tvGia.setText(mangSanPham.get(position).getGia().toString());
        holder.tvView.setText(mangSanPham.get(position).getView().toString());
        holder.tvTinhTrang.setText(mangSanPham.get(position).getTinhtrang());

        String image = mangSanPham.get(position).getHinh();
        String_To_ImageView(image, holder.ivIcon);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView ivIcon;
        TextView tvName, tvGia, tvView, tvTinhTrang;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivSPSearchIcon);
            tvName = (TextView) itemView.findViewById(R.id.tvSPName);
            tvGia = (TextView) itemView.findViewById(R.id.tvSPPrice);
            tvView = (TextView) itemView.findViewById(R.id.tvSPView);
            tvTinhTrang = (TextView) itemView.findViewById(R.id.tvSPTinhTrang);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            SharedPreferences settings = context.getSharedPreferences(MainActivity.getSanPham(),0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("id",mangSanPham.get(getAdapterPosition()).getId());
            editor.putString("name", mangSanPham.get(getAdapterPosition()).getName());
            editor.putInt("gia", mangSanPham.get(getAdapterPosition()).getGia());
            editor.putString("hinh", mangSanPham.get(getAdapterPosition()).getHinh());
            editor.putString("diachi", mangSanPham.get(getAdapterPosition()).getDiachi());
            editor.putString("chitiet", mangSanPham.get(getAdapterPosition()).getChitiet());
            editor.putString("tinhtrang", mangSanPham.get(getAdapterPosition()).getTinhtrang());
            editor.putInt("view",mangSanPham.get(getAdapterPosition()).getView());
            editor.putInt("user_id",mangSanPham.get(getAdapterPosition()).getUser_id());
            editor.putInt("loai_id",mangSanPham.get(getAdapterPosition()).getLoai_id());
            editor.commit();

            MainActivity main = (MainActivity) context;
            ((MainActivity) context).replaceHomeFragmentCTSP();
        }
    }

    public void String_To_ImageView(String strBase64, ImageView iv){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        iv.setImageBitmap(decodedByte);
    }

    @Override
    public int getItemCount() {
        return mangSanPham.size();
    }

    public void addItem(int position, SanPham sanPham){
        mangSanPham.add(position,sanPham);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangSanPham.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<SanPham> list){
        this.mangSanPham = list;
        notifyDataSetChanged();
    }
}
