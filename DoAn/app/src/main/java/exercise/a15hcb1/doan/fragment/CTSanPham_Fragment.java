package exercise.a15hcb1.doan.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.ChatRoom;
import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.SanPham;
import exercise.a15hcb1.doan.R;

import static exercise.a15hcb1.doan.R.id.btnCTSPCall;

/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class CTSanPham_Fragment extends Fragment implements View.OnClickListener {
    SanPham sanPham;
    Context context;
    TextView tv_CTSPName, tv_CTSPGia, tv_CTSPChiTiet, tv_CTSPTinhTrang, tv_CTSPDiaChi, tv_CTSPView;
    ImageView imageViewHinh;
    Button btnCall, btnChat, btnMsg, btnShare, btnFavorite;
    public static CTSanPham_Fragment newInstance(Context context) {
        CTSanPham_Fragment fm = new CTSanPham_Fragment();
        fm.context = context;
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ctsanpham_fragment, container, false);
        tv_CTSPName = (TextView) rootView.findViewById(R.id.tvCTSPName);
        tv_CTSPGia = (TextView) rootView.findViewById(R.id.tvCTSPPrice);
        tv_CTSPChiTiet = (TextView) rootView.findViewById(R.id.tvCTSPDesc);
        tv_CTSPTinhTrang = (TextView) rootView.findViewById(R.id.tvCTSPTinhTrang);
        tv_CTSPDiaChi = (TextView) rootView.findViewById(R.id.tvCTSPDiachi);
        tv_CTSPView = (TextView) rootView.findViewById(R.id.tvCTSPView);
        imageViewHinh = (ImageView) rootView.findViewById(R.id.ivCTSPIcon);

        btnCall = (Button) rootView.findViewById(btnCTSPCall);
        btnChat = (Button) rootView.findViewById(R.id.btnCTSPChat);
        btnMsg = (Button) rootView.findViewById(R.id.btnCTSPNhanTin);
        btnShare = (Button) rootView.findViewById(R.id.btnCTSPShare);
        btnFavorite = (Button) rootView.findViewById(R.id.btnCTSPLike);

        btnCall.setOnClickListener(this);
        btnChat.setOnClickListener(this);
        btnMsg.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnFavorite.setOnClickListener(this);



        SharedPreferences filesanpham = context.getSharedPreferences(MainActivity.getSanPham(),0);

        sanPham = new SanPham(
                filesanpham.getInt("id",0),
                filesanpham.getString("name",""),
                filesanpham.getInt("gia", 0),
                filesanpham.getString("hinh", ""),
                filesanpham.getString("diachi", ""),
                filesanpham.getString("chitiet",""),
                filesanpham.getString("tinhtrang", ""),
                filesanpham.getInt("view",0),
                filesanpham.getInt("user_id",0),
                filesanpham.getInt("loai_id",0)
        );

        tv_CTSPName.setText(sanPham.getName());
        tv_CTSPGia.setText(sanPham.getGia().toString());
        tv_CTSPDiaChi.setText(sanPham.getDiachi());
        tv_CTSPChiTiet.setText(sanPham.getChitiet());
        tv_CTSPTinhTrang.setText(sanPham.getTinhtrang());
        tv_CTSPView.setText(sanPham.getView().toString());
        String_To_ImageView(sanPham.getHinh(),imageViewHinh);

        new loadUser().execute(getString(R.string.url)+"User/"+sanPham.getUser_id());



        return rootView;
    }
    //bat su kien
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnCTSPCall){
            SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
            String phone = file_SanPham_User.getString("phone","");
            String tel = "tel:" + phone;
            Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse(tel));
            startActivity(dial);

        }else if(v.getId() == R.id.btnCTSPChat){
            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_id = file_User_Login.getInt("id",0);
            if(user_id != 0) {
                new KiemTraVaThem().execute();
            }else {
                Toast.makeText(context, "Vui lòng đăng nhập để tiếp tục", Toast.LENGTH_SHORT).show();
            }
        }else if(v.getId() == R.id.btnCTSPNhanTin){
            SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
            String phone = file_SanPham_User.getString("phone","");

            String tel = "smsto:" + phone;
            String noiDung = "San pham " + sanPham.getName();
            Intent sms = new Intent(Intent.ACTION_SENDTO,Uri.parse(tel));
            sms.putExtra("sms_body",noiDung);
            startActivity(sms);

        }else if(v.getId() == R.id.btnCTSPShare){

            //MainActivity main = (MainActivity) context;
            //main.sharePhotoToFacebook();
            //sharePhotoToFacebook();

        }else if(v.getId() == R.id.btnCTSPLike){
            //Integer id_sanpham = sanPham.getId();

            //SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            //Integer user_send_id = file_User_Login.getInt("id",0);
            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_id = file_User_Login.getInt("id",0);
            if(user_id != 0) {
                new KiemTraVaThemFavorite().execute();
            }else {
                Toast.makeText(context, "Vui lòng đăng nhập để tiếp tục", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void String_To_ImageView(String strBase64, ImageView iv){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        iv.setImageBitmap(decodedByte);
    }

    public class loadUser extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                    JSONObject object = new JSONObject(s);

                    SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);

                    SharedPreferences.Editor editor = file_SanPham_User.edit();
                    editor.putInt("id",object.getInt("id"));
                    editor.putString("name",object.getString("name"));
                    editor.putString("facebook_id",object.getString("facebook_id") );
                    editor.putString("email", object.getString("email"));
                    editor.putString("phone", object.getString("phone"));
                    editor.putString("chat_id", object.getString("chat_id"));

                    editor.commit();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }



    public class KiemTraVaThem extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
            Integer user_receive_id = file_SanPham_User.getInt("id",0);

            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_send_id = file_User_Login.getInt("id",0);
            String kq = GET_URL(getString(R.string.url)+"Group/"+ user_send_id + "/" + user_receive_id);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if(jsonArray.length() == 0){
                    SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
                    Integer user_receive_id = file_SanPham_User.getInt("id",0);

                    SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
                    Integer user_send_id = file_User_Login.getInt("id",0);
                    if(user_send_id != user_receive_id) {
                        //Them group load chat
                        new ThemGroup().execute(getString(R.string.url) + "Group");

                    }else {
                        Toast.makeText(context, "Ban khong the chat voi chinh ban", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    //da co group. load chat len
                    Toast.makeText(context, "Da co group", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, ChatRoom.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ThemGroup extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = POST_GROUP(params[0],"POST");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //them group vao file
            SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
            Integer user_receive_id = file_SanPham_User.getInt("id",0);

            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_send_id = file_User_Login.getInt("id",0);

            SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);
            SharedPreferences.Editor editor = group_chat.edit();

            editor.putInt("user_send_id",user_send_id);
            editor.putInt("user_receive_id",user_receive_id);

            editor.commit();

            new loadGroup().execute();
            /*
            Intent intent = new Intent(context, ChatRoom.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(context,"Them group thanh cong", Toast.LENGTH_SHORT).show();

            */

        }
    }

    private String POST_GROUP(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
        Integer user_receive_id = file_SanPham_User.getInt("id",0);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_send_id = file_User_Login.getInt("id",0);

        // Các tham số truyền
        List nameValuePair = new ArrayList(3);
        nameValuePair.add(new BasicNameValuePair("user_send_id", user_send_id.toString()));

        nameValuePair.add(new BasicNameValuePair("user_receive_id", user_receive_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    public class KiemTraVaThemFavorite extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq ="";
            SharedPreferences filesanpham = context.getSharedPreferences(MainActivity.getSanPham(),0);
            Integer sanpham_id = filesanpham.getInt("id", 0);

            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_id = file_User_Login.getInt("id",0);

            kq = GET_URL(getString(R.string.url)+"Favorite/" + user_id + "/" + sanpham_id);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if(jsonArray.length() == 0){
                    new ThemFavorite().execute(getString(R.string.url)+"Favorite");
                }else{
                    Toast.makeText(context,"San pham da co trong yeu thich", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public class ThemFavorite extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = POST_FAVORITE(params[0],"POST");
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context,"Them favorite thanh cong", Toast.LENGTH_SHORT).show();
        }
    }

    private String POST_FAVORITE(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        //SharedPreferences file_SanPham_User = context.getSharedPreferences(MainActivity.getSanPhamUser(),0);
        //Integer user_receive_id = file_SanPham_User.getInt("id",0);
        SharedPreferences filesanpham = context.getSharedPreferences(MainActivity.getSanPham(),0);
        Integer sanpham_id = filesanpham.getInt("id", 0);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_id = file_User_Login.getInt("id",0);

        // Các tham số truyền
        List nameValuePair = new ArrayList(3);
        nameValuePair.add(new BasicNameValuePair("user_id", user_id.toString()));

        nameValuePair.add(new BasicNameValuePair("sanpham_id", sanpham_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return kq;
    }

    public class loadGroup extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {

            SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);


            int user_send_id = group_chat.getInt("user_send_id",0);
            int user_receive_id = group_chat.getInt("user_receive_id",0);


            String kq = GET_URL(context.getString(R.string.url)+"Group/"+ user_send_id + "/" + user_receive_id);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);
                    SharedPreferences.Editor editor = group_chat.edit();

                    editor.putInt("id",object.getInt("id"));

                    editor.commit();

                    Intent intent = new Intent(context, ChatRoom.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(context,"Them group thanh cong", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
