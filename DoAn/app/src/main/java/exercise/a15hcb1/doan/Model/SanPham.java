package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 02/02/2017.
 */

public class SanPham {
    private Integer id;
    private String name;
    private Integer gia;
    private String hinh;
    private String diachi;
    private String chitiet;
    private String tinhtrang;
    private Integer view;
    private Integer user_id;
    private Integer loai_id;

    public SanPham(Integer id, String name, Integer gia, String hinh, String diachi, String chitiet, String tinhtrang, Integer view) {
        this.id = id;
        this.name = name;
        this.gia = gia;
        this.hinh = hinh;
        this.diachi = diachi;
        this.chitiet = chitiet;
        this.tinhtrang = tinhtrang;
        this.view = view;
    }

    public SanPham( String name, Integer gia, String hinh, String diachi, String chitiet, String tinhtrang, Integer view) {
        this.name = name;
        this.gia = gia;
        this.hinh = hinh;
        this.diachi = diachi;
        this.chitiet = chitiet;
        this.tinhtrang = tinhtrang;
        this.view = view;
    }

    public SanPham(Integer id, String name, Integer gia, String hinh, String diachi, String chitiet, String tinhtrang, Integer view, Integer user_id, Integer loai_id) {
        this.id = id;
        this.name = name;
        this.gia = gia;
        this.hinh = hinh;
        this.diachi = diachi;
        this.chitiet = chitiet;
        this.tinhtrang = tinhtrang;
        this.view = view;
        this.user_id = user_id;
        this.loai_id = loai_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGia() {
        return gia;
    }

    public void setGia(Integer gia) {
        this.gia = gia;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getChitiet() {
        return chitiet;
    }

    public void setChitiet(String chitiet) {
        this.chitiet = chitiet;
    }

    public String getTinhtrang() {
        return tinhtrang;
    }

    public void setTinhtrang(String tinhtrang) {
        this.tinhtrang = tinhtrang;
    }

    public Integer getView() {
        return view;
    }

    public void setView(Integer view) {
        this.view = view;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }


    public Integer getLoai_id() {
        return loai_id;
    }

    public void setLoai_id(Integer loai_id) {
        this.loai_id = loai_id;
    }
}
