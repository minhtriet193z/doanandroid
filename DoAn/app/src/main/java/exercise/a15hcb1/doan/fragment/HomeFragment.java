package exercise.a15hcb1.doan.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.MyFragmentPagerAdapter;

/**
 * Created by Administrator on 25/01/2017.
 */

public class HomeFragment extends Fragment implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener{
    ViewPager myViewPager;
    TabHost myTabHost;
    List<Fragment> myList = new ArrayList<Fragment>();
    MyFragmentPagerAdapter adapter;
    View v;

    public static HomeFragment newInstance(List<Fragment> myList) {
        HomeFragment fm = new HomeFragment();
        fm.myList = myList;
        return fm;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.layout_tabhost,container,false);

        initViewPager(myList);
        initTabHost();

        return v;
    }

    private void initTabHost() {
        myTabHost = (TabHost) v.findViewById(R.id.myTabHost);
        myTabHost.setup();

        String[] tabNames = {"Danh mục", "Yêu thích", "Bạn bè", "Group", "Người dùng"};

        for(int i=0; i<tabNames.length; i++){
            TabHost.TabSpec tabSpec = myTabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator(tabNames[i]);
            tabSpec.setContent(new FakeContent(getContext()));
            myTabHost.addTab(tabSpec);
            TextView tv = (TextView) myTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
            tv.setTextColor(Color.parseColor("#ffffff"));
            tv.setAllCaps(false);
        }

        myTabHost.setOnTabChangedListener(this);
    }

    private void initViewPager(List<Fragment> myList) {
        myViewPager = (ViewPager) v.findViewById(R.id.my_viewpager);

        adapter = new MyFragmentPagerAdapter(getChildFragmentManager(),myList);
        myViewPager.setAdapter(adapter);


        myViewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        myTabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String s) {
        int selected = myTabHost.getCurrentTab();
        myViewPager.setCurrentItem(selected);

        HorizontalScrollView hor = (HorizontalScrollView) v.findViewById(R.id.h_scroll_view);
        View tabView = myTabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft() - (hor.getWidth() - tabView.getWidth()) /2 ;
        hor.smoothScrollTo(scrollPos,0);
    }

    private class FakeContent implements TabHost.TabContentFactory {
        Context context;

        public FakeContent(Context context) {
            this.context = context;
        }

        @Override
        public View createTabContent(String s) {
            View v = new View(context);
            v.setMinimumHeight(0);
            v.setMinimumWidth(0);
            return v;
        }
    }
}
