package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.ChatRoom;
import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.Group;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 03/02/2017.
 */

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.RecyclerViewHolder>{
    List<Group> mangGroup;
    Context context;
    public GroupAdapter(List<Group> mangGroup, Context context) {
        this.mangGroup = mangGroup;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.groupchat_item,parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvGroupName.setText("Group " + mangGroup.get(position).getId());
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvGroupName;
        Button btnGCChat, btnGCDelete;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvGroupName = (TextView) itemView.findViewById(R.id.tvGCName);
            btnGCChat = (Button) itemView.findViewById(R.id.btnGCChat);
            btnGCDelete = (Button) itemView.findViewById(R.id.btnGCDelete);
            btnGCDelete.setOnClickListener(this);
            btnGCChat.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnGCChat){
                //load intent chat
                //luu group vao file

                Integer user_receive_id = mangGroup.get(getAdapterPosition()).getUser_send_id();

                Integer user_send_id = mangGroup.get(getAdapterPosition()).getUser_receive_id();

                Integer id = mangGroup.get(getAdapterPosition()).getId();

                SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);
                SharedPreferences.Editor editor = group_chat.edit();

                editor.putInt("user_send_id",user_send_id);
                editor.putInt("user_receive_id",user_receive_id);
                editor.putInt("id", id);

                editor.commit();

                Intent intent = new Intent(context, ChatRoom.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }else if(v.getId() == R.id.btnGCDelete){
                //xoa group
                Integer id = mangGroup.get(getAdapterPosition()).getId();
                new XoaGroup().execute(context.getString(R.string.url)+"Group/"+id);

                removeItem(getAdapterPosition());
            }
        }
    }

    public class XoaGroup extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String kq = POST_URL(params[0],"DELETE");
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context,"Da xoa group", Toast.LENGTH_SHORT).show();
        }
    }
    private String POST_URL(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        // Các tham số truyền
        List nameValuePair = new ArrayList(1);

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }
    @Override
    public int getItemCount() {
        return mangGroup.size();
    }
    public void addItem(int position, Group group){
        mangGroup.add(position,group);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangGroup.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<Group> list){
        this.mangGroup = list;
        notifyDataSetChanged();
    }
}
