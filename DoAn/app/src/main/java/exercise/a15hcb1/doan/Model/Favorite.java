package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 03/02/2017.
 */

public class Favorite {
    private Integer id;
    private Integer user_id;
    private Integer sanpham_id;

    public Favorite(Integer id, Integer user_id, Integer sanpham_id) {
        this.id = id;
        this.user_id = user_id;
        this.sanpham_id = sanpham_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getSanpham_id() {
        return sanpham_id;
    }

    public void setSanpham_id(Integer sanpham_id) {
        this.sanpham_id = sanpham_id;
    }
}
