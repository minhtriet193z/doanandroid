package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.ChatRoom;
import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.User;
import exercise.a15hcb1.doan.R;

/**
 * Created by Administrator on 03/02/2017.
 */

public class BanBeAdapter extends RecyclerView.Adapter<BanBeAdapter.RecyclerViewHolder> {
    List<User> mangUser;
    Context context;
    public BanBeAdapter(List<User> mangUser, Context context) {
        this.mangUser = mangUser;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.banbe_item,parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvName.setText(mangUser.get(position).getName());
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName;
        Button btnXoa, btnChat, btnCall;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvBBName);

            btnXoa = (Button) itemView.findViewById(R.id.btnBBDelete);
            btnXoa.setOnClickListener(this);
            btnChat = (Button) itemView.findViewById(R.id.btnBBChat);
            btnChat.setOnClickListener(this);
            btnCall = (Button) itemView.findViewById(R.id.btnBBCall);
            btnCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnBBCall){
                //Toast.makeText(context,"Call", Toast.LENGTH_SHORT).show();

                String phone = mangUser.get(getAdapterPosition()).getPhone();
                String tel = "tel:" + phone;
                Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse(tel));
                dial.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(dial);

            }else if(v.getId() == R.id.btnBBChat){

                SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
                SharedPreferences.Editor editor = user_friend.edit();
                    editor.putInt("id",mangUser.get(getAdapterPosition()).getId());
                editor.commit();

                new KiemTraVaThem().execute();

            }else if(v.getId() == R.id.btnBBDelete){
                //Toast.makeText(context,"Xoa", Toast.LENGTH_SHORT).show();
                Integer friend_id = mangUser.get(getAdapterPosition()).getId();

                SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
                Integer user_id = file_User_Login.getInt("id",0);

                new xoaFriend().execute(context.getString(R.string.url) + "FriendXoa/" + user_id + "/" + friend_id);

                removeItem(getAdapterPosition());
            }
        }
    }

    public class xoaFriend extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            String kq = GET_URL(params[0]);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context,"Da xoa friend", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getItemCount() {
        return mangUser.size();
    }

    public void addItem(int position, User user){
        mangUser.add(position,user);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangUser.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<User> list){
        this.mangUser = list;
        notifyDataSetChanged();
    }

    public class KiemTraVaThem extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
            Integer user_receive_id = user_friend.getInt("id", 0);

            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_send_id = file_User_Login.getInt("id",0);

            String kq = GET_URL(context.getString(R.string.url)+"Group/"+ user_send_id + "/" + user_receive_id);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                if(jsonArray.length() == 0){
                    //Them group load chat
                    SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
                    Integer user_receive_id = user_friend.getInt("id", 0);

                    SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
                    Integer user_send_id = file_User_Login.getInt("id",0);

                    if(user_send_id != user_receive_id) {
                        new ThemGroup().execute(context.getString(R.string.url) + "Group");
                    }else {
                        Toast.makeText(context, "Ban khong the chat voi chinh ban", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    //da co group. load chat len
                    Toast.makeText(context, "Da co group", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, ChatRoom.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ThemGroup extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = POST_GROUP(params[0],"POST");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //luu group vao file
            SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
            Integer user_receive_id = user_friend.getInt("id", 0);

            SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
            Integer user_send_id = file_User_Login.getInt("id",0);

            SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);
            SharedPreferences.Editor editor = group_chat.edit();

            editor.putInt("user_send_id",user_send_id);
            editor.putInt("user_receive_id",user_receive_id);

            editor.commit();

            new loadGroup().execute();

            Intent intent = new Intent(context, ChatRoom.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            Toast.makeText(context,"Them group thanh cong", Toast.LENGTH_SHORT).show();
        }
    }
    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
    private String POST_GROUP(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);

        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request

        HttpPost httpPost = new HttpPost(url);

        SharedPreferences user_friend = context.getSharedPreferences(MainActivity.getUserFriend(), 0);
        Integer user_receive_id = user_friend.getInt("id", 0);

        SharedPreferences file_User_Login = context.getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_send_id = file_User_Login.getInt("id",0);

        // Các tham số truyền
        List nameValuePair = new ArrayList(3);
        nameValuePair.add(new BasicNameValuePair("user_send_id", user_send_id.toString()));

        nameValuePair.add(new BasicNameValuePair("user_receive_id", user_receive_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    public class loadGroup extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {

            SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);


            int user_send_id = group_chat.getInt("user_send_id",0);
            int user_receive_id = group_chat.getInt("user_receive_id",0);


            String kq = GET_URL(context.getString(R.string.url)+"Group/"+ user_send_id + "/" + user_receive_id);
            return kq;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    SharedPreferences group_chat = context.getSharedPreferences(MainActivity.getGroup_Chat(), 0);
                    SharedPreferences.Editor editor = group_chat.edit();

                    editor.putInt("id",object.getInt("id"));

                    editor.commit();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
