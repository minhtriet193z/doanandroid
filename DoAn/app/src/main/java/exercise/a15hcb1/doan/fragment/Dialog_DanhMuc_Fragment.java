package exercise.a15hcb1.doan.fragment;

import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.Model.LoaiSanPham;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.adapter.DialogDanhMucAdapter;

/**
 * Created by Administrator on 02/02/2017.
 */

public class Dialog_DanhMuc_Fragment extends DialogFragment {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    List<LoaiSanPham> mangDanhMuc;
    DialogDanhMucAdapter adapter;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.dialog_danhmuc, container, true);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_dialog_danhmuc);
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        mangDanhMuc = new ArrayList<LoaiSanPham>();

        new loadDanhMuc().execute(getString(R.string.url)+"LoaiSanPham");

        this.getDialog().setTitle("Danh mục sản phẩm");
        return view;
    }


    public class loadDanhMuc extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return GET_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);
                    mangDanhMuc.add(new LoaiSanPham(
                            object.getInt("id"),
                            object.getString("alias"),
                            object.getString("name"),
                            object.getString("description"),
                            object.getString("hinh")
                    ));
                }

                adapter = new DialogDanhMucAdapter(mangDanhMuc, view.getContext());
                recyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static String GET_URL(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        try
        {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }
}
