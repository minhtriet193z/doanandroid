package exercise.a15hcb1.doan.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.MainActivity;
import exercise.a15hcb1.doan.Model.SanPham;
import exercise.a15hcb1.doan.R;
import exercise.a15hcb1.doan.SuaSanPham;

/**
 * Created by Administrator on 02/02/2017.
 */

public class SanPhamEditAdapter extends RecyclerView.Adapter<SanPhamEditAdapter.RecyclerViewHolder> {
    List<SanPham> mangSanPham;
    Context context;
    View v;
    public SanPhamEditAdapter(List<SanPham> mangSanPham, Context context) {
        this.mangSanPham = mangSanPham;
        this.context = context;
    }

    @Override
    public SanPhamEditAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        v = inflater.inflate(R.layout.sanpham_edit_item,parent, false);

        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SanPhamEditAdapter.RecyclerViewHolder holder, int position) {
        holder.tvName.setText(mangSanPham.get(position).getName());
        holder.tvGia.setText(mangSanPham.get(position).getGia().toString());
        String image = mangSanPham.get(position).getHinh();
        String_To_ImageView(image, holder.ivIcon);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView ivIcon;
        TextView tvName, tvGia;
        Button btnEdit, btnDelete;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivSPSearchIcon);
            tvName = (TextView) itemView.findViewById(R.id.tvSPEditName);
            tvGia = (TextView) itemView.findViewById(R.id.tvSPEditPrice);
            btnEdit = (Button) itemView.findViewById(R.id.btnSanPhamEdit);
            btnDelete = (Button) itemView.findViewById(R.id.btnSanPhamDelete);

            btnEdit.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnSanPhamEdit){
                SharedPreferences settings = context.getSharedPreferences(MainActivity.getSanPham(),0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("id",mangSanPham.get(getAdapterPosition()).getId());
                editor.putString("name", mangSanPham.get(getAdapterPosition()).getName());
                editor.putInt("gia", mangSanPham.get(getAdapterPosition()).getGia());
                editor.putString("hinh", mangSanPham.get(getAdapterPosition()).getHinh());
                editor.putString("diachi", mangSanPham.get(getAdapterPosition()).getDiachi());
                editor.putString("chitiet", mangSanPham.get(getAdapterPosition()).getChitiet());
                editor.putString("tinhtrang", mangSanPham.get(getAdapterPosition()).getTinhtrang());
                editor.putInt("view",mangSanPham.get(getAdapterPosition()).getView());
                editor.putInt("user_id",mangSanPham.get(getAdapterPosition()).getUser_id());
                editor.putInt("loai_id",mangSanPham.get(getAdapterPosition()).getLoai_id());
                editor.commit();
                Toast.makeText(context, "Edit san pham", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, SuaSanPham.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }else if(v.getId() == R.id.btnSanPhamDelete){
                xoaSanPham(mangSanPham.get(getAdapterPosition()).getId());
            }
        }
    }
    public void xoaSanPham(Integer id){
        new XoaSP().execute(context.getString(R.string.url)+"SanPham/"+id);
    }

    public class XoaSP extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = POST_URL(params[0],"DELETE");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(context, "Xoa sản phẩm thành công", Toast.LENGTH_SHORT).show();
        }
    }

    private String POST_URL(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);
        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request
        HttpPost httpPost = new HttpPost(url);

        // Các tham số truyền
        List nameValuePair = new ArrayList(1);

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    public void String_To_ImageView(String strBase64, ImageView iv){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        iv.setImageBitmap(decodedByte);
    }

    @Override
    public int getItemCount() {
        return mangSanPham.size();
    }

    public void addItem(int position, SanPham sanPham){
        mangSanPham.add(position,sanPham);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mangSanPham.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(List<SanPham> list){
        this.mangSanPham = list;
        notifyDataSetChanged();
    }
}
