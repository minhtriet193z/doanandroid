package exercise.a15hcb1.doan;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import exercise.a15hcb1.doan.fragment.Dialog_DanhMuc_Fragment;

public class SuaSanPham extends AppCompatActivity implements View.OnClickListener {

    Button btnDanhMuc, btnSua;
    EditText edName, edGia, edChiTiet, edTinhTrang, edDiaChi;
    ImageView ivhinh;
    final Integer REQUEST_CODE = 321;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sua_san_pham);

        anhXa();

        SharedPreferences sanPham = getSharedPreferences(MainActivity.getSanPham(),0);
        edName.setText(sanPham.getString("name",""));
        edGia.setText(String.valueOf(sanPham.getInt("gia",0)));
        edChiTiet.setText(sanPham.getString("chitiet",""));
        edTinhTrang.setText(sanPham.getString("tinhtrang",""));
        edDiaChi.setText(sanPham.getString("diachi",""));
        String_To_ImageView(sanPham.getString("hinh",""),ivhinh);

        ivhinh.setOnClickListener(this);
        btnDanhMuc.setOnClickListener(this);
        btnSua.setOnClickListener(this);

    }

    public void anhXa(){
        btnSua = (Button) findViewById(R.id.btnEditSP_Add);
        btnDanhMuc = (Button) findViewById(R.id.btnEditSP_Danhmuc);
        edName = (EditText) findViewById(R.id.edEditSP_Name);
        edGia = (EditText) findViewById(R.id.edEditSP_Gia);
        edChiTiet = (EditText) findViewById(R.id.edEditSP_Chitiet);
        edTinhTrang = (EditText) findViewById(R.id.edEditSP_TinhTrang);
        edDiaChi = (EditText) findViewById(R.id.edEditSP_Diachi);
        ivhinh = (ImageView) findViewById(R.id.ivEditSP_Hinh);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.ivEditSP_Hinh) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CODE);
        }else if( v.getId() == R.id.btnEditSP_Danhmuc){
            showDialogFragment();
        }else if( v.getId() == R.id.btnEditSP_Add){

            suaSanPham();
        }
    }

    private void suaSanPham() {
        SharedPreferences settings = getSharedPreferences(MainActivity.getDanhMuc(),0);
        Integer danhmuc = settings.getInt("add_danhmuc_id",0);
        if(danhmuc == 0){
            Toast.makeText(this,"Bạn chưa chọn danh mục", Toast.LENGTH_SHORT).show();
        } else {
            SharedPreferences sanPham = getSharedPreferences(MainActivity.getSanPham(),0);
            Integer id = sanPham.getInt("id",0);
            new SuaSP().execute(getString(R.string.url)+"SanPham/"+id);
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            Bitmap bmp = (Bitmap) data.getExtras().get("data");
            ivhinh.setImageBitmap(bmp);
            ivhinh.setBackgroundColor(Color.WHITE);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void showDialogFragment() {
        FragmentManager fm = getFragmentManager();
        Dialog_DanhMuc_Fragment dialogFragment= new Dialog_DanhMuc_Fragment();
        dialogFragment.show(fm,"News Fragment");
    }

    public class SuaSP extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = POST_URL(params[0],"PUT");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplication(), "Sửa sản phẩm thành công", Toast.LENGTH_SHORT).show();
            SharedPreferences settings = getSharedPreferences(MainActivity.getDanhMuc(), 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("add_danhmuc_id",0);
            editor.commit();
        }
    }

    private String POST_URL(String url, String type) {
        android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
        android.os.StrictMode.setThreadPolicy(policy);
        HttpClient httpClient = new DefaultHttpClient();

        // URL của trang web nhận request
        HttpPost httpPost = new HttpPost(url);

        // Các tham số truyền
        List nameValuePair = new ArrayList(10);
        nameValuePair.add(new BasicNameValuePair("name", edName.getText().toString()));

        nameValuePair.add(new BasicNameValuePair("gia", edGia.getText().toString()));

        String hinh = ImageView_To_String(ivhinh);
        nameValuePair.add(new BasicNameValuePair("hinh", hinh));

        nameValuePair.add(new BasicNameValuePair("diachi", edDiaChi.getText().toString()));

        nameValuePair.add(new BasicNameValuePair("chitiet", edChiTiet.getText().toString()));

        nameValuePair.add(new BasicNameValuePair("tinhtrang", edTinhTrang.getText().toString()));

        nameValuePair.add(new BasicNameValuePair("view", "0"));

        SharedPreferences settings = getSharedPreferences(MainActivity.getFileUser(),0);
        Integer user_id = settings.getInt("id",0);
        nameValuePair.add(new BasicNameValuePair("user_id", user_id.toString()));

        settings = getSharedPreferences(MainActivity.getDanhMuc(),0);
        Integer danhmuc_id = settings.getInt("add_danhmuc_id",0);
        nameValuePair.add(new BasicNameValuePair("loai_id", danhmuc_id.toString()));

        nameValuePair.add(new BasicNameValuePair("_method", type));

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String kq = "";
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            kq = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kq;
    }

    public String ImageView_To_String(ImageView iv){
        BitmapDrawable drawable = (BitmapDrawable) iv.getDrawable();
        Bitmap bmp = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        String strHinh = Base64.encodeToString(byteArray, 0);
        return strHinh;
    }

    public void String_To_ImageView(String strBase64, ImageView iv){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        iv.setImageBitmap(decodedByte);
    }
}
