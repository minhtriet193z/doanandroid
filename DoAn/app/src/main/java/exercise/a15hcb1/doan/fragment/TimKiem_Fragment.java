package exercise.a15hcb1.doan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import exercise.a15hcb1.doan.R;
/**
 * Created by Dell 3542 on 29/01/2017.
 */

public class TimKiem_Fragment extends Fragment {

    public static TimKiem_Fragment newInstance() {
        TimKiem_Fragment fm = new TimKiem_Fragment();
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.timkiem_fragment, container, false);

        return rootView;
    }
}
