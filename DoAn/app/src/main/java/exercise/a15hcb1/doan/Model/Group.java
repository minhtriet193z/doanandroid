package exercise.a15hcb1.doan.Model;

/**
 * Created by Administrator on 03/02/2017.
 */

public class Group {
    private Integer user_send_id;
    private Integer user_receive_id;
    private Integer id;

    public Group(Integer id, Integer user_send_id, Integer user_receive_id) {
        this.user_send_id = user_send_id;
        this.user_receive_id = user_receive_id;
        this.id = id;
    }

    public Integer getUser_send_id() {
        return user_send_id;
    }

    public void setUser_send_id(Integer user_send_id) {
        this.user_send_id = user_send_id;
    }

    public Integer getUser_receive_id() {
        return user_receive_id;
    }

    public void setUser_receive_id(Integer user_receive_id) {
        this.user_receive_id = user_receive_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
