package exercise.a15hcb1.doan.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 20/12/2016.
 */

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> myList;

    public MyFragmentPagerAdapter(FragmentManager fm, List<Fragment> myList) {
        super(fm);
        this.myList = myList;
    }

    @Override
    public Fragment getItem(int position) {
        return myList.get(position);
    }

    @Override
    public int getCount() {
        return myList.size();
    }
}
